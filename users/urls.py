from django.urls import path, include
from . import views
from rest_framework import routers
from users.views import *


router = routers.DefaultRouter()
router.register('users', views.CustomUserView)


urlpatterns = [
    path('', include(router.urls)),
    path('signup/', views.SignUp.as_view(), name='signup'),


    path('<int:id>/details/', employee_details, name="employee_details"),
    path('<int:id>/edit/', employee_edit, name="employee_edit"),
    path('add/', employee_add, name="employee_add"),
    path('<int:id>/delete/', employee_delete, name="employee_delete"),
]
