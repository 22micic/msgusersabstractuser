from django.contrib.auth import authenticate
from django.contrib.auth.models import AbstractUser
from rest_framework import serializers, exceptions
from .models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'phone', 'address')


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AbstractUser
        fields = ['username', 'first_name',
                  'last_name', 'profile', 'email',
                  'is_staff', 'is_active', 'date_joined',
                  'is_superuser']


class LoginSerializer(serializers.ModelSerializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        username = data.get("username", "")
        password = data.get("password", "")

        if username and password:
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    data["user"] = user
                else:
                    una = "User is deactivated."
                    raise exceptions.ValidationError(una)
            else:
                una = "Unable to login with given credentials."
                raise exceptions.ValidationError(una)
        else:
            una = "Must provide username and password both."
            raise exceptions.ValidationError(una)
        return data
