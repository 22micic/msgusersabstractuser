from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'
    label = 'my_users'
